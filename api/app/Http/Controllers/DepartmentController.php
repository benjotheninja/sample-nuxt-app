<?php

namespace App\Http\Controllers;

use App\Http\Resources\DepartmentResource;
use App\Models\Department;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        $resource = DepartmentResource::collection($departments);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);

        $department = new Department;
        $department->name = $request->name;
        $department->save();

        $resource = new DepartmentResource($department);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::find($id);
        if($department === null)
            return response('/GET: Department with id of "'.$id.'" not found.', 404);

        $resource = new DepartmentResource($department);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);
        
        $department = Department::find($id);
        if($department === null)
            return response('/UPDATE: Department with id of "'.$id.'" not found.', 404);

        $department->name = $request->name;
        $department->save();

        $resource = new DepartmentResource($department);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::find($id);
        if($department === null)
            return response('/DELETE: Department with id of "'.$id.'" not found.', 404);

        $department->delete();
        
        $resource = new DepartmentResource($department);
        $result = $resource;

        return response($result, 200);
    }
}
