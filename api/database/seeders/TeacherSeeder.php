<?php

namespace Database\Seeders;

use App\Models\Teacher;
use Illuminate\Database\Seeder;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Teacher::factory(10)->create();

        $data = [
            ['name' => 'Prof. Dennis Ritchie', 'department_id' => 1],
            ['name' => 'Prof. Leonardo da Vinci', 'department_id' => 2],
            ['name' => 'Prof. Jean Piaget', 'department_id' => 3],
        ];

        foreach($data as $teacher) {
            $teachers = new Teacher;
            $teachers->name = $teacher['name'];
            $teachers->department_id = $teacher['department_id'];
            $teachers->save();
        }
    }
}
