<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $teacher = $this->teacher;
        $subject = $this->subject;
        $room = $this->room;

        return [
            'id' => $this->id,
            'teacher' => [
                'id' => $teacher->id,
                'name' => $teacher->name
            ],
            'subject' => [
                'id' => $subject->id,
                'name' => $subject->name
            ],
            'room' => [
                'id' => $room->id,
                'name' => $room->name,
                'bldg' => $room->bldg
            ],
            'time' => [
                'start' => $this->time_start,
                'end' => $this->time_end
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
