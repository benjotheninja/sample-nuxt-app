<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    /**
     * Get the course logs of the student.
     */
    public function courseLogs() {
        return $this->hasMany(CourseLog::class);
    }

    /**
     * Get the schedules of the student.
     */
    public function schedules() {
        return $this->hasMany(Schedule::class);
    }
}
