<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoomResource;
use App\Models\Room;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::all();
        $resource = RoomResource::collection($rooms);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);

        $room = new Room;
        $room->name = $request->name;
        $room->save();

        $resource = new RoomResource($room);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $room = Room::find($id);
        if($room === null)
            return response('/GET: Room with id of "'.$id.'" not found.', 404);

        $resource = new RoomResource($room);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);
        
        $room = Room::find($id);
        if($room === null)
            return response('/UPDATE: Room with id of "'.$id.'" not found.', 404);

        $room->name = $request->name;
        $room->save();

        $resource = new RoomResource($room);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::find($id);
        if($room === null)
            return response('/DELETE: Room with id of "'.$id.'" not found.', 404);

        $room->delete();
        
        $resource = new RoomResource($room);
        $result = $resource;

        return response($result, 200);
    }
}
