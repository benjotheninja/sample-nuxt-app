export const state = () => ({
  list: []
})

export const mutations = {
  load(state, courses) {
    // load courses
    state.list = courses
  }
}