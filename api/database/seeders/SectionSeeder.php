<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Section;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'teacher_id' => 1,
                'subject_id' => 1,
                'room_id' => 1,
                'time_start' => '08:00:00',
                'time_end' => '09:00:00'
            ],
            [
                'teacher_id' => 1,
                'subject_id' => 2,
                'room_id' => 1,
                'time_start' => '09:00:00',
                'time_end' => '10:00:00'
            ],
            [
                'teacher_id' => 1,
                'subject_id' => 3,
                'room_id' => 4,
                'time_start' => '11:00:00',
                'time_end' => '12:00:00'
            ],
            [
                'teacher_id' => 2,
                'subject_id' => 5,
                'room_id' => 4,
                'time_start' => '08:00:00',
                'time_end' => '09:00:00'
            ],
            [
                'teacher_id' => 2,
                'subject_id' => 6,
                'room_id' => 1,
                'time_start' => '13:00:00',
                'time_end' => '14:00:00'
            ],
            [
                'teacher_id' => 3,
                'subject_id' => 9,
                'room_id' => 6,
                'time_start' => '10:00:00',
                'time_end' => '11:00:00'
            ],
            [
                'teacher_id' => 3,
                'subject_id' => 10,
                'room_id' => 5,
                'time_start' => '14:00:00',
                'time_end' => '15:00:00'
            ],
            [
                'teacher_id' => 3,
                'subject_id' => 11,
                'room_id' => 8,
                'time_start' => '15:00:00',
                'time_end' => '16:00:00'
            ],
            [
                'teacher_id' => 3,
                'subject_id' => 12,
                'room_id' => 9,
                'time_start' => '16:00:00',
                'time_end' => '17:00:00'
            ],
            
        ];

        foreach($data as $section) {
            $sections = new Section;
            $sections->teacher_id = $section['teacher_id'];
            $sections->subject_id = $section['subject_id'];
            $sections->room_id = $section['room_id'];
            $sections->time_start = $section['time_start'];
            $sections->time_end = $section['time_end'];
            $sections->save();
        }
    }
}
