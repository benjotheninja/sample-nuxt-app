<?php

namespace App\Http\Controllers;

use App\Http\Resources\SubjectResource;
use App\Models\Subject;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all();
        $resource = SubjectResource::collection($subjects);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);

        $subject = new Subject;
        $subject->name = $request->name;
        $subject->save();

        $resource = new SubjectResource($subject);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = Subject::find($id);
        if($subject === null)
            return response('/GET: Subject with id of "'.$id.'" not found.', 404);

        $resource = new SubjectResource($subject);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);
        
        $subject = Subject::find($id);
        if($subject === null)
            return response('/UPDATE: Subject with id of "'.$id.'" not found.', 404);

        $subject->name = $request->name;
        $subject->save();

        $resource = new SubjectResource($subject);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = Subject::find($id);
        if($subject === null)
            return response('/DELETE: Subject with id of "'.$id.'" not found.', 404);

        $subject->delete();
        
        $resource = new SubjectResource($subject);
        $result = $resource;

        return response($result, 200);
    }
}
