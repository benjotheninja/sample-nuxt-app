<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Degree;

class DegreeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Degree::factory(3)->create();

        $data = [
            [
                'name' => 'Bachelor of Science',
                'acro' => 'BS'
            ],
            [
                'name' => 'Bachelor of Fine Arts',
                'acro' => 'BFA'
            ],
            [
                'name' => 'Bachelor of Arts',
                'acro' => 'AB'
            ]
        ];

        foreach($data as $degree) {
            $degrees = new Degree;
            $degrees->name = $degree['name'];
            $degrees->acro = $degree['acro'];
            $degrees->save();
        }
    }
}
