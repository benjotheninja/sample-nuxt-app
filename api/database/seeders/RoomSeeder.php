<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Room;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Room::factory(10)->create();

        $data = [
            ['name' => 'CS112', 'bldg' => 'DPT Building'],
            ['name' => 'CS103', 'bldg' => 'DPT Building'],
            ['name' => 'CS202', 'bldg' => 'DPT Building'],
            ['name' => 'CS501', 'bldg' => 'DPT Building'],
            ['name' => 'FA771', 'bldg' => 'GET Building'],
            ['name' => 'FA101', 'bldg' => 'GET Building'],
            ['name' => 'FA102', 'bldg' => 'GET Building'],
            ['name' => 'FA503', 'bldg' => 'GET Building'],
            ['name' => 'PS701', 'bldg' => 'BE Building'],
            ['name' => 'PS801', 'bldg' => 'BE Building'],
            ['name' => 'PS802', 'bldg' => 'BE Building'],
            ['name' => 'PS503', 'bldg' => 'BE Building'],
        ];

        foreach($data as $room) {
            $rooms = new Room;
            $rooms->name = $room['name'];
            $rooms->bldg = $room['bldg'];
            $rooms->save();
        }
    }
}
