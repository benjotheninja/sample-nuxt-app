<?php

namespace App\Http\Controllers;

use App\Http\Resources\CourseResource;
use App\Models\Course;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::all();
        $resource = CourseResource::collection($courses);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'department_id' => ['required', 'integer']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);

        $course = new Course;
        $course->name = $request->name;
        $course->department_id = $request->department_id;
        $course->save();

        $resource = new CourseResource($course);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::find($id);
        if($course === null)
            return response('/GET: Course with id of "'.$id.'" not found.', 404);

        $resource = new CourseResource($course);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'department_id' => ['required', 'integer']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);
        
        $course = Course::find($id);
        if($course === null)
            return response('/UPDATE: Course with id of "'.$id.'" not found.', 404);

        $course->name = $request->name;
        $course->department_id = $request->department_id;
        $course->save();

        $resource = new CourseResource($course);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        if($course === null)
            return response('/DELETE: Course with id of "'.$id.'" not found.', 404);

        $course->delete();
        
        $resource = new CourseResource($course);
        $result = $resource;

        return response($result, 200);
    }
}
