<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// controllers
use App\Http\Controllers\CourseLogController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\DegreeController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\UserController;

use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources([
    'course-logs' => CourseLogController::class,
    'courses' => CourseController::class,
    'degree' => DegreeController::class,
    'department' => DepartmentController::class,
    'rooms' => RoomController::class,
    'schedules' => ScheduleController::class,
    'sections' => SectionController::class,
    'students' => StudentController::class,
    'subjects' => SubjectController::class,
    'teachers' => TeacherController::class,
    'users' => UserController::class
]);

Route::post('login', LoginController::class);