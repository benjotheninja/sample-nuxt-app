<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $courseLog = $this->courseLogs;
        $courseLog = $courseLog->sortByDesc('created_at')->first();
        $course = $courseLog->course;
        $degree = $course->degree;
        $department = $course->department;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'course' => [
                'id' => $course->id,
                'name' => $course->name,
                'degree' => [
                    'id' => $degree->id,
                    'name' => $degree->name,
                    'acro' => $degree->acro
                ],
                'department' => [
                    'id' => $department->id,
                    'name' => $department->name,
                    'acro' => $department->acro
                ]
            ],
            'schedules' => ScheduleResource::collection($this->schedules),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
