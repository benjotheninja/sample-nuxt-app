<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;

    /**
     * Get the teacher that owns the section.
     */
    public function teacher() {
        return $this->belongsTo(Teacher::class);
    }

    /**
     * Get the subject that owns the section.
     */
    public function subject() {
        return $this->belongsTo(Subject::class);
    }

    /**
     * Get the room that owns the section.
     */
    public function room() {
        return $this->belongsTo(Room::class);
    }

    /**
     * Get the schedules of the section.
     */
    public function schedules() {
        return $this->hasMany(Schedule::class);
    }
}
