<?php

namespace App\Http\Controllers;

use App\Http\Resources\ScheduleResource;
use App\Models\Schedule;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::all();
        $resource = ScheduleResource::collection($schedules);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'section_id' => ['required', 'integer'],
            'student_id' => ['required', 'integer']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);

        $schedule = new Schedule;
        $schedule->section_id = $request->section_id;
        $schedule->student_id = $request->student_id;
        $schedule->save();

        $resource = new ScheduleResource($schedule);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedule = Schedule::find($id);
        if($schedule === null)
            return response('/GET: Schedule with id of "'.$id.'" not found.', 404);

        $resource = new ScheduleResource($schedule);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'section_id' => ['required', 'integer'],
            'student_id' => ['required', 'integer']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);
        
        $schedule = Schedule::find($id);
        if($schedule === null)
            return response('/UPDATE: Schedule with id of "'.$id.'" not found.', 404);

        $schedule->section_id = $request->section_id;
        $schedule->student_id = $request->student_id;
        $schedule->save();

        $resource = new ScheduleResource($schedule);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = Schedule::find($id);
        if($schedule === null)
            return response('/DELETE: Schedule with id of "'.$id.'" not found.', 404);

        $schedule->delete();
        
        $resource = new ScheduleResource($schedule);
        $result = $resource;

        return response($result, 200);
    }
}
