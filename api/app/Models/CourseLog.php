<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseLog extends Model
{
    use HasFactory;

    /**
     * Get the course that owns the course logs.
     */
    public function course() {
        return $this->belongsTo(Course::class);
    }

    /**
     * Get the student that owns the course logs.
     */
    public function student() {
        return $this->belongsTo(Student::class);
    }
}
