<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $section = $this->section;
        $student = $this->student;

        $teacher = $section->teacher;
        $subject = $section->subject;
        $room = $section->room;

        $department = $teacher->department;

        return [
            'id' => $this->id,
            'section' => [
                'id' => $section->id,
                'teacher' => [
                    'id' => $teacher->id,
                    'name' => $teacher->name,
                    'department' => [
                        'id' => $department->id,
                        'name' => $department->name
                    ]
                ],
                'subject' => [
                    'id' => $subject->id,
                    'name' => $subject->name
                ],
                'room' => [
                    'id' => $room->id,
                    'name' => $room->name,
                    'bldg' => $room->bldg
                ],
                'time' => [
                    'start' => $section->time_start,
                    'end' => $section->time_end
                ]
            ],
            'student' => [
                'id' => $student->id,
                'name' => $student->name
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
