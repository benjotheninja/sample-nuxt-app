<?php

namespace App\Http\Controllers;

use App\Http\Resources\CourseLogResource;
use App\Models\CourseLog;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourseLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courseLogs = CourseLog::all();
        $resource = CourseLogResource::collection($courseLogs);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'course_id' => ['required', 'integer'],
            'student_id' => ['required', 'integer']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);

        $courseLog = new CourseLog;
        $courseLog->course_id = $request->course_id;
        $courseLog->student_id = $request->student_id;
        $courseLog->save();

        $resource = new CourseLogResource($courseLog);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CourseLog  $courseLog
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $courseLog = CourseLog::find($id);
        if($courseLog === null)
            return response('/GET: CourseLog with id of "'.$id.'" not found.', 404);

        $resource = new CourseLogResource($courseLog);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CourseLog  $courseLog
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseLog $courseLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CourseLog  $courseLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'course_id' => ['required', 'integer'],
            'student_id' => ['required', 'integer']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);
        
        $courseLog = CourseLog::find($id);
        if($courseLog === null)
            return response('/UPDATE: CourseLog with id of "'.$id.'" not found.', 404);

        $courseLog->course_id = $request->course_id;
        $courseLog->student_id = $request->student_id;
        $courseLog->save();

        $resource = new CourseLogResource($courseLog);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CourseLog  $courseLog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $courseLog = CourseLog::find($id);
        if($courseLog === null)
            return response('/DELETE: CourseLog with id of "'.$id.'" not found.', 404);

        $courseLog->delete();
        
        $resource = new CourseLogResource($courseLog);
        $result = $resource;

        return response($result, 200);
    }
}
