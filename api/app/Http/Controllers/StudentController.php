<?php

namespace App\Http\Controllers;

use App\Http\Resources\StudentResource;
use App\Models\CourseLog;
use App\Models\Schedule;
use App\Models\Student;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        $resource = StudentResource::collection($students);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'course_id' => ['required', 'integer']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);

        // save student
        $student = new Student;
        $student->name = $request->name;
        $student->save();

        // save course
        $courseLog = new CourseLog;
        $courseLog->course_id = $request->course_id;
        $courseLog->student_id = $student->id;
        $courseLog->save();

        $resource = new StudentResource($student);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);
        if($student === null)
            return response('/GET: Student with id of "'.$id.'" not found.', 404);

        $resource = new StudentResource($student);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'course_id' => ['required', 'integer']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);
        
        $student = Student::find($id);
        if($student === null)
            return response('/UPDATE: Student with id of "'.$id.'" not found.', 404);

        $student->name = $request->name;
        $student->save();

        // save course
        $courseLog = new CourseLog;
        $courseLog->course_id = $request->course_id;
        $courseLog->student_id = $student->id;
        $courseLog->save();

        $resource = new StudentResource($student);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        if($student === null)
            return response('/DELETE: Student with id of "'.$id.'" not found.', 404);

        $student->delete();
        
        // remove associated data
        CourseLog::where('student_id', $student->id)->delete();
        Schedule::where('student_id', $student->id)->delete();

        return response($student, 200);
    }
}
