<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Department::factory(5)->create();

        $data = [
            [
                'name' => 'College of Computing Education',
                'acro' => 'CCE'
            ],
            [
                'name' => 'College of Architecture and Fine Arts Education',
                'acro' => 'CAFAE'
            ],
            [
                'name' => 'College of Arts and Sciences Education',
                'acro' => 'CASE'
            ]
        ];

        foreach($data as $department) {
            $departments = new Department;
            $departments->name = $department['name'];
            $departments->acro = $department['acro'];
            $departments->save();
        }
    }
}
