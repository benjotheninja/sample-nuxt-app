<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    /**
     * Get the teachers of the department.
     */
    public function teachers() {
        return $this->hasMany(Teacher::class);
    }

    /**
     * Get the courses of the department.
     */
    public function courses() {
        return $this->hasMany(Course::class);
    }
}
