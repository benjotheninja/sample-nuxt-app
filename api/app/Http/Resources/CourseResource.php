<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $degree = $this->degree;
        $department = $this->department;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'degree' => [
                'id' => $degree->id,
                'name' => $degree->name,
                'acro' => $degree->acro
            ],
            'department' => [
                'id' => $department->id,
                'name' => $department->name,
                'acro' => $department->acro
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
