<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\Course::factory(10)->create();
        // \App\Models\Degree::factory(3)->create();
        // \App\Models\Department::factory(5)->create();
        // \App\Models\Room::factory(10)->create();
        // \App\Models\Student::factory(10)->create();
        // \App\Models\Subject::factory(10)->create();
        // \App\Models\Teacher::factory(10)->create();

        $this->call([
            CourseLogSeeder::class,
            CourseSeeder::class,
            DegreeSeeder::class,
            DepartmentSeeder::class,
            RoomSeeder::class,
            ScheduleSeeder::class,
            SectionSeeder::class,
            StudentSeeder::class,
            SubjectSeeder::class,
            TeacherSeeder::class
        ]);
    }
}
