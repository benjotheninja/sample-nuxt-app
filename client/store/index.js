export const actions = {
  async nuxtServerInit ({ dispatch }) {
    try {
      await dispatch('students')
      await dispatch('sections')
      await dispatch('courses')
    } catch (error) {
      // notify through console
      console.log(`Cannot connect to database. ${error}`)
    }
  },

  async students({ commit }) {
    const students = await this.$axios.$get('students')
    commit('students/load', students)
  },

  async sections({ commit }) {
    const sections = await this.$axios.$get('sections')
    commit('sections/load', sections)
  },

  async courses({ commit }) {
    const courses = await this.$axios.$get('courses')
    commit('courses/load', courses)
  }
}
