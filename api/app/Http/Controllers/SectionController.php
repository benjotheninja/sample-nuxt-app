<?php

namespace App\Http\Controllers;

use App\Http\Resources\SectionResource;
use App\Models\Section;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = Section::all();
        $resource = SectionResource::collection($sections);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'teacher_id' => ['required', 'integer'],
            'subject_id' => ['required', 'integer'],
            'room_id' => ['required', 'integer'],
            'time_start' => ['required'],
            'time_end' => ['required']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);

        $section = new Section;
        $section->teacher_id = $request->teacher_id;
        $section->subject_id = $request->subject_id;
        $section->room_id = $request->room_id;
        $section->time_start = $request->time_start;
        $section->time_end = $request->time_end;
        $section->save();

        $resource = new SectionResource($section);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $section = Section::find($id);
        if($section === null)
            return response('/GET: Section with id of "'.$id.'" not found.', 404);

        $resource = new SectionResource($section);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'teacher_id' => ['required', 'integer'],
            'subject_id' => ['required', 'integer'],
            'room_id' => ['required', 'integer'],
            'time_start' => ['required'],
            'time_end' => ['required']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);
        
        $section = Section::find($id);
        if($section === null)
            return response('/UPDATE: Section with id of "'.$id.'" not found.', 404);

        $section->teacher_id = $request->teacher_id;
        $section->subject_id = $request->subject_id;
        $section->room_id = $request->room_id;
        $section->time_start = $request->time_start;
        $section->time_end = $request->time_end;
        $section->save();

        $resource = new SectionResource($section);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Section::find($id);
        if($section === null)
            return response('/DELETE: Section with id of "'.$id.'" not found.', 404);

        $section->delete();
        
        $resource = new SectionResource($section);
        $result = $resource;

        return response($result, 200);
    }
}
