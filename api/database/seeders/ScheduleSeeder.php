<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Schedule;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['section_id' => 1, 'student_id' => 1],
            ['section_id' => 2, 'student_id' => 1],
            ['section_id' => 3, 'student_id' => 1],
            ['section_id' => 4, 'student_id' => 2],
            ['section_id' => 5, 'student_id' => 2],
            ['section_id' => 6, 'student_id' => 3],
            ['section_id' => 7, 'student_id' => 3],
            ['section_id' => 8, 'student_id' => 3],
            ['section_id' => 9, 'student_id' => 3],
        ];

        foreach($data as $schedule) {
            $schedules = new Schedule;
            $schedules->section_id = $schedule['section_id'];
            $schedules->student_id = $schedule['student_id'];
            $schedules->save();
        }
    }
}
