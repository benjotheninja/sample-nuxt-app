<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Course::factory(10)->create();

        $data = [
            [
                'name' => 'Computer Science',
                'degree_id' => 1,
                'department_id' => 1
            ],
            [
                'name' => 'Information Technology',
                'degree_id' => 1,
                'department_id' => 1
            ],
            [
                'name' => 'Painting',
                'degree_id' => 2,
                'department_id' => 2
            ],
            [
                'name' => 'Architecture',
                'degree_id' => 2,
                'department_id' => 2
            ],
            [
                'name' => 'Psychology',
                'degree_id' => 1,
                'department_id' => 3
            ],
            [
                'name' => 'Psychology',
                'degree_id' => 3,
                'department_id' => 1
            ]
        ];

        foreach($data as $course) {
            $courses = new Course;
            $courses->name = $course['name'];
            $courses->degree_id = $course['degree_id'];
            $courses->department_id = $course['department_id'];
            $courses->save();
        }
    }
}
