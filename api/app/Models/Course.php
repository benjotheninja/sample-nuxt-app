<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    /**
     * Get the department that owns the course.
     */
    public function department() {
        return $this->belongsTo(Department::class);
    }

    /**
     * Get the degree that owns the course.
     */
    public function degree() {
        return $this->belongsTo(Degree::class);
    }

    /**
     * Get the course logs of the course.
     */
    public function courseLogs() {
        return $this->hasMany(CourseLog::class);
    }
}
