<?php

namespace App\Http\Controllers;

use App\Http\Resources\TeacherResource;
use App\Models\Teacher;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::all();
        $resource = TeacherResource::collection($teachers);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'department_id' => ['required', 'integer']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);

        $teacher = new Teacher;
        $teacher->name = $request->name;
        $teacher->department_id = $request->department_id;
        $teacher->save();

        $resource = new TeacherResource($teacher);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::find($id);
        if($teacher === null)
            return response('/GET: Teacher with id of "'.$id.'" not found.', 404);

        $resource = new TeacherResource($teacher);
        $result = $resource;

        return response($result, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'department_id' => ['required', 'integer']
        ]);

        if($validator->fails())
            return response($validator->errors(), 406);
        
        $teacher = Teacher::find($id);
        if($teacher === null)
            return response('/UPDATE: Teacher with id of "'.$id.'" not found.', 404);

        $teacher->name = $request->name;
        $teacher->department_id = $request->department_id;
        $teacher->save();

        $resource = new TeacherResource($teacher);
        $result = $resource;

        return response($result, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = Teacher::find($id);
        if($teacher === null)
            return response('/DELETE: Teacher with id of "'.$id.'" not found.', 404);

        $teacher->delete();
        
        $resource = new TeacherResource($teacher);
        $result = $resource;

        return response($result, 200);
    }
}
