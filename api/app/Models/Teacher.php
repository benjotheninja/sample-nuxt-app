<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;

    /**
     * Get the department that owns the course.
     */
    public function department() {
        return $this->belongsTo(Department::class);
    }

    /**
     * Get the sections of the teacher.
     */
    public function sections() {
        return $this->hasMany(Section::class);
    }
}
