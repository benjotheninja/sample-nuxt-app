<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    /**
     * Get the section that owns the schedule.
     */
    public function section() {
        return $this->belongsTo(Section::class);
    }

    /**
     * Get the student that owns the schedule.
     */
    public function student() {
        return $this->belongsTo(Student::class);
    }
}
