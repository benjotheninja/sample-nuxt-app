<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Subject::factory(10)->create();

        $data = [
            'Logic Formulation',
            'Data Structure',
            'Object-Oriented Programming',
            'Programming 101',
            'Basic Sketching',
            'Color Mixing',
            'Scene Framing',
            'Painting 101',
            'Psychology Introduction',
            'Human Anatomy',
            'Understanding Behaviours',
            'Psychology 101'
        ];

        foreach($data as $subject) {
            $subjects = new Subject;
            $subjects->name = $subject;
            $subjects->save();
        }
    }
}
