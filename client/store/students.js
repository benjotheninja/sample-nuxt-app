export const state = () => ({
  list: []
})

export const mutations = {
  load(state, students) {
    // load students
    state.list = students
  },

  create(state, student) {
    // push the new student
    state.list.push(student)
  },

  update(state, updatedStudent) {
    // update student from list
    state.list = state.list.map(student => {
      if(student.id === updatedStudent.id)
        return updatedStudent

      // return student as is
      return student
    })
  },

  remove(state, studentId) {
    // remove student from list
    const index = state.list.map(student => student.id).indexOf(studentId)
    state.list.splice(index, 1)
  }
}