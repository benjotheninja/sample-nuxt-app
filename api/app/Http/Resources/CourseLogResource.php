<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $student = $this->student;
        $course = $this->course;
        $department = $course->department;

        return [
            'id' => $this->id,
            'course' => [
                'id' => $course->id,
                'name' => $course->name,
                'department' => [
                    'id' => $department->id,
                    'name' => $department->name,
                ]
            ],
            'student' => [
                'id' => $student->id,
                'name' => $student->name,
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
