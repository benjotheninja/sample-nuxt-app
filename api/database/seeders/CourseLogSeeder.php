<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CourseLog;

class CourseLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['course_id' => 1, 'student_id' => 1],
            ['course_id' => 3, 'student_id' => 2],
            ['course_id' => 5, 'student_id' => 3]
        ];

        foreach($data as $courseLog) {
            $courseLogs = new CourseLog;
            $courseLogs->course_id = $courseLog['course_id'];
            $courseLogs->student_id = $courseLog['student_id'];
            $courseLogs->save();
        }
    }
}
