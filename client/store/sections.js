export const state = () => ({
  list: []
})

export const mutations = {
  load(state, sections) {
    // load sections
    state.list = sections
  }
}